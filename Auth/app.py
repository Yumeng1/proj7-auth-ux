"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
from itsdangerous import (TimedJSONWebSignatureSerializer \
                                  as Serializer, BadSignature, \
                                  SignatureExpired)
import random
import os
import flask
import pymongo
from flask import request, render_template, url_for, redirect, Flask, flash, session, jsonify, abort, Response
from flask_login import (LoginManager, current_user, login_required,
        login_user, logout_user, UserMixin,
        confirm_login, fresh_login_required)
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient
import logging
from flask_restful import Resource, Api
import json
import csv
from password import hash_password, verify_password
from wtforms import Form, BooleanField, StringField, validators, SubmitField
from flask_wtf import FlaskForm
from wtforms.validators import DataRequired
import time


###
# Globals
###
app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

db2 = client.usersdb
###
# Pages
###

###
# https://zhuanlan.zhihu.com/p/56689952
# https://segmentfault.com/a/1190000013535016




class User(UserMixin):
    def __init__(self, username, password, id_user, active = True):
        self.username = username
        self.password = password
        self.id_user = id_user
        self.active = active

    def is_active(self):
        return self.active

    def get_id(self):
        return self.id_user

user_store_list = []

login_manager = LoginManager()

login_manager.login_view = 'login'
login_manager.login_message = 'Please log in to access this page'
login_manager.refresh_view = 'reauth'
login_manager.setup_app(app)

@login_manager.user_loader
def load_user(id_user):
    for user in user_store_list:
        if str(user.id_user) == id_user:
            return user
        return None

##############################################################
#most of this part is from testToken.py
#https://blog.csdn.net/qq_35891226/article/details/79931210
def generate_auth_token(id, expiration=600):
    s = Serializer(app.secret_key, expires_in=expiration)
    token = s.dumps({'id_user': id})
    return {'token': token, 'duration': expiration}


def verify_auth_token(token):
    s = Serializer(app.secret_key)
    try:
        data = s.loads(token)
    except SignatureExpired:
        return None    
    except BadSignature:
        return None    
    return "Success"

#################################################################################

class Loginform(FlaskForm):
    username = StringField('username', validators = [validators.DataRequired(), validators.Length(min = 4, max = 25)])
    password = StringField('password', validators = [validators.DataRequired(), validators.Length(min = 6, max = 20)])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('log in')

class RegistrationForm(FlaskForm):
    username = StringField('username', validators = [validators.DataRequired(), validators.Length(min = 2, max = 25)])
    password = StringField('password', validators = [validators.DataRequired(), validators.Length(min = 2, max = 20)])
    # again_pass = StringField('check Password', validators=[DataRequired(), EqualTo('password', message='Password should same')])
    submit = SubmitField('register')

#################################################################################
# the following code comes from https://www.cnblogs.com/vovlie/p/4182814.html
# to supply the hash to encrypt the password and verify the password
############################################################################################
# some of the folllowing method are from: https://flask-login.readthedocs.io/en/latest/
# and also https://wiki.jikexueyuan.com/project/php-basics-notes/cookie-and-session.html

@app.route('/login', methods=('GET', 'POST'))
def login():
    """
 login part, to check if user log in, most from slide
    """
    global user_store_list
    form = Loginform()
    app.logger.debug("login process begin!!!")
    if form.validate_on_submit():
        username = form.username.data
        password = form.username.data
        app.logger.debug("good for first if")

        for each in user_store_list:
            app.logger.debug("good for first for")
            if each.username == username and verify_password(password, each.password):
                remember = form.remember_me.data
                if login_user(each, remember=remember):
                    token_apply = generate_auth_token(each.id_user,600)
                    session['token'] = token_apply
                    flash('You have been successfully logged in')
                    return redirect(url_for('index'))

    return render_template('login.html', form=form)

@app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out.")
    session['token'] = None
    return redirect(url_for("index"))

@app.errorhandler(401)
def error401(error):
    return Response('No token', 401)

#######################################################################################
def randid():
    # in ordre to get different user id 
    re = random.randint(1,1000)
    return re


@app.route('/register', methods=('GET', 'POST'))
def register():
    # store id with random index numbe
    global user_store_list, db2
    form = RegistrationForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        username = form.username.data
        password = hash_password(form.username.data)
        indexid = randid()

        users_id_dic = {
            "username": username,
            "password": password,
            "id_user": indexid
        }

        db2.usersdb.insert_one(users_id_dic)

        user_data = db2.usersdb.find()
        for each in user_data:
            user_store_list.append(User(each['username'],each['password'],each['id_user']))

        msg = jsonify(username = username, password = password, id_user = indexid)
        msg.status_code = 201
        
        return msg

    return render_template('register.html', form=form),400
############################################################################
@app.route('/token', methods = ['GET'])
def get_token():    
    tooke = session['token']
    
    if verify_auth_token(tooke) =='Success':
        return jsonify(duration = 600, token=token), 201
    abort(401)

#################################################################################################################
@app.route("/")
@app.route("/index")
def index():
    # global token_check
    # if verify_auth_token(token_check) != "Success":
    #         return redirect(url_for('login'))
    # elif verify_auth_token(token_check) == "Success":
        return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    km = request.args.get('km', 999, type=int)
    distance = request.args.get('brevet_disk_km', 999, type = int)
    date = request.args.get('begin_date')
    time = request.args.get('begin_time')

    app.logger.debug("Got a JSON request")
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    
    time_combine = date + "T" + time 
    
    re_time = arrow.get(time_combine).isoformat()
    
    open_time = acp_times.open_time(km, distance, re_time)
    close_time = acp_times.close_time(km, distance, re_time)
    
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############
@app.route('/error')
def error():
    return render_template('error.html')

@app.route('/todo', methods=['POST'])
def todo():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('todo.html', items=items)

@app.route('/new', methods=['POST'])
def new():
    km = request.form.getlist('km')
    distance = request.form.getlist('distance')
    o_time = request.form.getlist('open')
    c_time = request.form.getlist('close')


    km_list = []
    distance_list = []
    open_list = []
    close_list = []

    for data in km:
        if data != '':
            km_list.append(data)
    
    for data in distance:
        if data != '':
            distance_list.append(data)
        
    for data in o_time:
        if data != '':
            open_list.append(data)
    
    for data in c_time:
        if data != '':
            close_list.append(data)
        
    length = len(open_list)

    for i in range(length):
        result_data = {
            'km' : km_list[i],
            'distance' : distance_list[0],
            'open' : open_list[i],
            'close' : close_list[i]
        }
        db.tododb.insert_one(result_data)

    return redirect(url_for('index'))


############################################################################################################
class list_all(Resource):
    def get(self):
        # https://www.bufeihua.cn/p/562c719432e3a80001800050
        # using sort according to the link above
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     abort(401)
        # elif verify_auth_token(token_check) == "Success":

        list_data = db.tododb.find().sort('open', pymongo.ASCENDING)
        items = [item for item in list_data]

        result_open = []
        result_close = []

        for item in items:
            result_open.append(item['open'])
            result_close.append(item['close'])
        
        result_dic = {}
        result_dic['open'] = result_open
        result_dic['close'] = result_close
        
        return result_dic
    
class list_open_only(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            top = request.args.get('top')
            if top == None:
                top = 20
            list_data = db.tododb.find().sort('open', pymongo.ASCENDING).limit(top)
            items = [item for item in list_data]

            open_list_only = []

            for item in items:
                open_list_only.append(item['open'])
            
            result_dic = {}
            result_dic['open'] = open_list_only
            
            return result_dic
    
class list_close_only(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            top = request.args.get('top')
            if top == None:
                top = 20
            list_data = db.tododb.find().sort('close', pymongo.ASCENDING).limit(top)
            items = [item for item in list_data]

            close_list_only = []

            for item in items:
                close_list_only.append(item['close'])
            
            result_dic = {}
            result_dic['close'] = close_list_only
            
            return result_dic
#####################################################################################

class list_all_csv(Resource):
    def list_all_csv(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            list_data = db.tododb.find().sort('open', pymongo.ASCENDING)
            result = " "
            for item in list_data:
                result += "open time" +  "  " + item['open'] + ", " + "close time" +  "  " + item['close'] + "; "
            return result

class list_all_json(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            list_data = db.tododb.find().sort('open', pymongo.ASCENDING)
            items = [item for item in list_data]

            result_open = []
            result_close = []

            for item in items:
                result_open.append(item['open'])
                result_close.append(item['close'])
            
            result_dic = {}
            result_dic['open'] = result_open
            result_dic['close'] = result_close
            
            return result_dic

##########################################################################################################

class list_open_csv(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            n = request.args.get('top', type = int)
            if n == None:
                n = 20
            list_data = db.tododb.find().sort('open', pymongo.ASCENDING).limit(n)
            result = " "
            for item in list_data:
                result += "open time" + "  " + item['open'] + ", "
            return result

class list_open_json(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            top = request.args.get('top')
            if top == None:
                top = 20
            list_data = db.tododb.find().sort('open', pymongo.ASCENDING).limit(top)
            items = [item for item in list_data]

            open_list_only = []

            for item in items:
                open_list_only.append(item['open'])
            
            result_dic = {}
            result_dic['open'] = open_list_only
            
            return result_dic        


###########################################################################################################

class list_close_csv(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            n = request.args.get('top', type = int)
            if n == None:
                list_data = db.tododb.find().sort('close', pymongo.ASCENDING)
                result = " "
                for item in list_data:
                    result += "close time" +  "  " + item['close'] + ", "
                return result
            else:
                result = " "
                temp = db.tododb.find().sort('close', pymongo.ASCENDING).limit(n)
                for item in temp:
                    result += "close time" + item['close'] + ", "
                return result

class list_close_json(Resource):
    def get(self):
        # token_check = session['token']
        # if verify_auth_token(token_check) != "Success":
        #     return redirect(url_for('login'))
        # elif verify_auth_token(token_check) == "Success":
            top = request.args.get('top')
            if top == None:
                top = 20
            list_data = db.tododb.find().sort('close', pymongo.ASCENDING).limit(top)
            items = [item for item in list_data]

            close_list_only = []

            for item in items:
                close_list_only.append(item['close'])
            
            result_dic = {}
            result_dic['close'] = close_list_only
            
            return result_dic  
    

############################################################################################################
    
# class list_alln_format(Resource):
#     def get(self,format):
#         n = request.args.get('top', type = int)
#         if format == 'csv':
#             return list_alln_csv(n)
#         if format == 'json':
#             return list_alln_json(n)

# class list_open_n_format(Resource):
#     def get(self,format):
#         n = request.args.get('top',type = int)
#         if format == 'csv':
#             return list_open_n_csv(n)
#         if format == 'json':
#             return list_open_n_json(n)

# class list_close_n_format(Resource):
#     def get(self,format):
#         n = request.args.get('top',type = int)
#         if format == 'csv':
#             return list_close_n_csv(n)
#         if format == 'json':
#             return list_close_n_json(n)

# def list_alln_csv(n):
#     result = " "
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result += "open time" + item['open'] + "close time" + item['close'] + ", "
#     return result

# def list_open_n_csv(n):
#     result = " "
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result += "open time" + item['open'] + ", "
#     return result

# def list_close_n_csv(n):
#     result = " "
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result += "close time" + item['close'] + ", "
#     return result
    
# def list_alln_json(n):
#     result = []
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result.append({'open':item['open'], 'close':item['close']})
#     return result

# def list_open_n_json(n):
#     result = []
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result.append({'open':item['open']})  
#     return result

# def list_close_n_json(n):
#     result = []
#     temp = db.tododb.find().sort({'line': 1}).limit(n)
#     for item in temp:
#         result.append({'close':item['close']})
#     return result


api.add_resource(list_all,'/listAll')
api.add_resource(list_open_only,'/listOpenOnly')
api.add_resource(list_close_only,'/listCloseOnly')

api.add_resource(list_all_csv,'/listAll/csv')
api.add_resource(list_all_json,'/listAll/json')

api.add_resource(list_open_csv,'/listOpenOnly/csv')
api.add_resource(list_open_json,'/listOpenOnly/json')

api.add_resource(list_close_csv,'/listCloseOnly/csv')
api.add_resource(list_close_json,'/listCloseOnly/json')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
